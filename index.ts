import express, { Request, Response } from "express"
import dotenv from "dotenv"
dotenv.config()

import { Sequelize } from "sequelize"

const sequelize = new Sequelize(`${process.env.DB_URI}`)

const app = express()


app.get("/", (req: Request, res: Response) => {
  res.json("its actually awesome!")
})

app.get("/bad", (req: Request, res: Response) => {
  res.json("soooo hard!")
})

app.get("/good", (req: Request, res: Response) => {
  res.json("good!")
})

const port = process.env.PORT || 9000

app.listen(port, () => {
  console.log("App listening at http://localhost:" + port)
})