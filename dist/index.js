"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config();
const app = express_1.default();
app.get("/", (req, res) => {
    res.json("its actually awesome!");
});
app.get("/bad", (req, res) => {
    res.json("soooo hard!");
});
const port = process.env.PORT || 9000;
app.listen(port, () => {
    console.log("App listening at http://localhost:" + port);
});
